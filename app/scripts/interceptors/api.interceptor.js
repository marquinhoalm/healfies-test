(function() {
    'use strict';

    angular
        .module('app')
        .service('ApiInterceptor', ApiInterceptor);

    /** @ngInject */
    function ApiInterceptor($rootScope) {

        var service = this;

        service.responseError = function(response) {
            if (response.status === 400 ||
                response.status === 401 ||
                response.status === 500 ||
                response.status === 404
            ) {
                $rootScope.$broadcast('requesterror', response);
            }
            return response;
        };
    }
})();
