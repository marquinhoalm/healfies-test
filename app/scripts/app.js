'use strict';

angular
	.module('app', [
		'ngAnimate',
		'ngCookies',
		'ngResource',
		'ngSanitize',
		'ngStorage',
		'ui.router',
		'angularMoment',
		'ngMaterial',
		'ngLodash'
	]);

angular
	.module('app')
	.constant('API_URL', 'https://sheetsu.com/apis');

angular.module('app').config(['$mdThemingProvider', function($mdThemingProvider) {
	var customPrimary = {
		'50': '#9477c4',
		'100': '#8666bc',
		'200': '#7854b5',
		'300': '#6c48a7',
		'400': '#614195',
		'500': '#553983',
		'600': '#493171',
		'700': '#3e2a5f',
		'800': '#32224e',
		'900': '#271a3c',
		'A100': '#a289cc',
		'A200': '#b09bd3',
		'A400': '#beaddb',
		'A700': '#1b122a'
	};
	$mdThemingProvider
		.definePalette('customPrimary',
			customPrimary);

	var customAccent = {
		'50': '#ccbfe3',
		'100': '#beaddb',
		'200': '#b09bd3',
		'300': '#a289cc',
		'400': '#9478c4',
		'500': '#8666BC',
		'600': '#7854b4',
		'700': '#6c49a6',
		'800': '#604194',
		'900': '#553983',
		'A100': '#dad0eb',
		'A200': '#e8e2f2',
		'A400': '#f6f4fa',
		'A700': '#493171'
	};
	$mdThemingProvider
		.definePalette('customAccent',
			customAccent);

	var customWarn = {
		'50': '#ff8080',
		'100': '#ff6666',
		'200': '#ff4d4d',
		'300': '#ff3333',
		'400': '#ff1a1a',
		'500': '#f93159',
		'600': '#e60000',
		'700': '#cc0000',
		'800': '#b30000',
		'900': '#990000',
		'A100': '#ff9999',
		'A200': '#ffb3b3',
		'A400': '#ffcccc',
		'A700': '#800000'
	};
	$mdThemingProvider
		.definePalette('customWarn',
			customWarn);

	var customBackground = {
		'50': '#ffffff',
		'100': '#f7f7f7',
		'200': '#eaeaea',
		'300': '#dddddd',
		'400': '#d1d1d1',
		'500': '#c4c4c4',
		'600': '#b7b7b7',
		'700': '#aaaaaa',
		'800': '#9e9e9e',
		'900': '#919191',
		'A100': '#ffffff',
		'A200': '#ffffff',
		'A400': '#ffffff',
		'A700': '#848484'
	};

	$mdThemingProvider
		.definePalette('customBackground',
			customBackground);

	$mdThemingProvider.theme('default')
		.primaryPalette('customPrimary')
		.accentPalette('customAccent')
		.warnPalette('customWarn')
		.backgroundPalette('customBackground')

}]);

angular
	.module('app')
	.config(['$httpProvider', function($httpProvider) {
		$httpProvider.interceptors.push('ApiInterceptor');
	}]);

angular
	.module('app')
	.run(['$rootScope', 'amMoment', 'PeopleService', 'MainService', 'CommentsService', 'FlashService', function($rootScope, amMoment, PeopleService, MainService, CommentsService, FlashService) {
		amMoment.changeLocale('pt-br');
		PeopleService.getPeople()
			.then(function(result) {
					MainService.setCurrentUser(PeopleService.getPerson("1"));
				},
				function(error) {

				});
		$rootScope
			.$on(
				'$stateChangeError',
				function(event, toState, toParams, fromState, fromParams, error) {
					console.log(error);
				});

		$rootScope
			.$on(
				'requesterror',
				function(error, response) {
					if (response.data) {
						FlashService.show(response.data);
					}
				});
	}]);
