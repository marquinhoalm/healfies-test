(function() {
    'use strict';

    angular
        .module('app')
        .factory('FlashService', FlashService);

    /** @ngInject */
    function FlashService($mdDialog, $mdToast) {
        return {
            message: function(message) {
                $mdToast.show(
                    $mdToast.simple()
                    .textContent(message)
                    .position('bottom right')
                    .hideDelay(3000)
                );
            },

            dialog: function(message, title) {
                $mdDialog.show(
                    $mdDialog.alert()
                    .parent(angular.element(document.body))
                    .clickOutsideToClose(true)
                    .title(title === undefined ? 'Aviso!' : title)
                    .content(message)
                    .ariaLabel(message)
                    .ok('Fechar')
                );
            }
        }
    }
})();
