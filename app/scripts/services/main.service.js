(function() {
    'use strict';

    angular
        .module('app')
        .factory('MainService', MainService);

    /** @ngInject */
    function MainService(SessionService) {
        var vm = this;

        vm.getCurrentUser = function() {
            var user = SessionService.get('current_user');
            return user;
        };

        vm.setCurrentUser = function(user) {
            SessionService.set('current_user', user);
        };

        return vm;
    }

})();
