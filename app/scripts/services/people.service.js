(function() {
	'use strict';

	angular.module('app').service('PeopleService', PeopleService);

	/** @ngInject */
	function PeopleService($q, API_URL, $http, lodash, FlashService) {
		var vm = this;

		vm.people = [];

		vm.getPerson = function(id) {
			var person = lodash.find(vm.people, function(p, v) {
				return p.id === id;
			});

			return person;
		};

		vm.getPeople = function() {
			var deferred = $q.defer();

			var req = {
				method: 'GET',
				url: API_URL + "/652c7bb7",
				headers: {
					'Content-Type': 'application/json'
				}
			};

			$http(req).then(
				function(response) {
					if (response.data.status !== 200) {
						FlashService.message(response.data.error);
						deferred.reject(response.data.error);
					} else {
						vm.people = response.data.result;
						deferred.resolve(vm.people);
					}
				}
			);

			return deferred.promise;
		};
	};
})()
