(function() {
    'use strict';

    angular
        .module('app')
        .factory('SessionService', SessionService);

    SessionService.$inject = ['$sessionStorage'];

    /** @ngInject */
    function SessionService($sessionStorage) {
        return {
            get: function(key) {
                return $sessionStorage[key];
            },
            set: function(key, val) {
                $sessionStorage[key] = val;
            },
            unset: function(key) {
                delete $sessionStorage[key];
            },
            clear: function() {
                $sessionStorage.$reset();
            }
        };
    }
})();
