(function() {
	'use strict';

	angular.module('app').service('CommentsService', CommentsService);

	/** @ngInject */
	function CommentsService($q, API_URL, $http, lodash) {
		var vm = this;

		vm.commentsList = [];

		vm.get = function(note) {
			var comment = lodash.filter(vm.commentsList, function(c, v) {
				return c.post === note;
			});

			return comment;
		};

		vm.getAll = function() {
			var deferred = $q.defer();

			var req = {
				method: 'GET',
				url: API_URL + "/6b76822e",
				headers: {
					'Content-Type': 'application/json'
				}
			};

			$http(req).then(
				function(response) {
					if (response.data.status !== 200) {
						deferred.reject(response.data);
					} else {
						vm.commentsList = response.data.result;
						deferred.resolve(response.data.result);
					}
				},
				function(error) {
					deferred.reject(error.data);
				}
			);

			return deferred.promise;
		}

		vm.create = function(comment) {
			var deferred = $q.defer();

			var req = {
				method: 'POST',
				url: API_URL + "/6b76822e",
				headers: {
					'Content-Type': 'application/json'
				},
				data: comment
			};

			$http(req).then(
				function(response) {
					if (response.data.status !== 201) {
						deferred.reject(response.data);
					} else {
						deferred.resolve(response.data.result);
					}
				},
				function(error) {
					deferred.reject(error.data);
				}
			);

			return deferred.promise;
		}
	}
})();
