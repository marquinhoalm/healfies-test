(function() {
	'use strict';

	angular.module('app').service('NotesService', NotesService);

	/** @ngInject */
	function NotesService($q, API_URL, $http) {
		var vm = this;

		vm.getNotes = function() {
			var deferred = $q.defer();

			var req = {
				method: 'GET',
				url: API_URL + "/90e15674",
				headers: {
					'Content-Type': 'application/json'
				}
			};

			$http(req).then(
				function(response) {
					if (response.data.status !== 200) {
						deferred.reject(response.data);
					} else {
						deferred.resolve(response.data.result);
					}
				},
				function(error) {
					deferred.reject(error.data);
				}
			);

			return deferred.promise;
		};

		vm.create = function(note) {
			var deferred = $q.defer();

			var req = {
				method: 'POST',
				url: API_URL + "/90e15674",
				headers: {
					'Content-Type': 'application/json'
				},
				data: note
			};

			$http(req).then(
				function(response) {
					if (response.data.status !== 201) {
						deferred.reject(response.data);
					} else {
						deferred.resolve(response.data.result);
					}
				},
				function(error) {
					deferred.reject(error.data);
				}
			);

			return deferred.promise;
		}
	}


})();
