(function() {
	'use strict';

	angular.module('app').controller('NotesListController', NotesListController);

	/** @ngInject */
	function NotesListController(
		$mdDialog, $log, NotesService, FlashService, PeopleService,
		CommentsService, MainService, moment
	) {
		var vm = this;

		vm.finishLoad = true;

		vm.openMoreOptions = function($mdOpenMenu, ev) {
			$mdOpenMenu(ev);
		};

		vm.loadPerson = function(person) {
			return PeopleService.getPerson(person);
		};

		vm.processingNoteList = function() {
			angular.forEach(vm.notesList, function(value, key) {
				var sharedTemp = [];
				value.person = vm.loadPerson(value.owner);
				value.files = value.files.trim().split(',');
				if (value.files[0] === "")
					value.files = undefined;
				value.folders = value.folders.trim().split(",");
				if (value.folders[0] === "")
					value.folders = undefined;
				value.shares = value.shares.split(",");

				angular.forEach(vm.notesList, function(valueNote, keyNote) {
					valueNote.comments = CommentsService.get(valueNote.id);
				});

				angular.forEach(value.shares, function(valueShare, key) {
					var person = vm.loadPerson(valueShare.trim());
					if (person !== undefined)
						sharedTemp.push(person);
				});

				value.shares = sharedTemp;
			});
		}

		vm.loadNotes = function() {
			return new Promise(function(resolve, reject) {
				NotesService.getNotes().then(
					function(resultNotes) {
						vm.notesList = resultNotes;
						vm.processingNoteList()
						resolve(vm.noteList);
					},
					function(error) {
						FlashService.message(error);
						reject(error);
					});
			});
		};

		vm.processingCommentList = function() {
			angular.forEach(vm.commentsList, function(value, key) {
				value.created_by = vm.loadPerson(value.created_by.trim());
			});
		}

		vm.loadComments = function() {
			return new Promise(function(resolve, reject) {
				CommentsService.getAll()
					.then(function(resultComments) {
							vm.commentsList = resultComments;
							vm.processingCommentList()

							resolve(vm.commentsList);
						},
						function(error) {
							FlashService.message(error);
							reject(error);
						});
			});
		};

		vm.loadData = function() {
			vm.loadComments().then(function(resultComment) {
				vm.loadNotes().then(function(resultNotes) {})
			})
		}

		vm.createComment = function(selectedNote, index) {
			if (vm.newComment[index] !== "") {
				vm.finishLoad = false;
				var idGenerate = vm.commentsList.length + 200;
				var comment = {
					id: idGenerate.toString(),
					post: selectedNote.id,
					content: vm.newComment[index],
					created_by: "1",
					created_at: moment().format('DD/MM/YYYY').toString()
				};

				CommentsService
					.create(comment)
					.then(function(resultCreateComment) {
						comment.created_by = PeopleService.getPerson("1")
						vm.commentsList.push(comment);
						selectedNote.comments.push(comment);
						vm.finishLoad = true;
						vm.newComment[index] = "";
						FlashService.message("Comentário Criado!");
					}, function(error) {
						vm.finishLoad = true;
						FlashService.message("Deu Ruim no Comentário!");
					});
			}
		};

		vm.createNote = function(ev) {
			$mdDialog.show({
				controller: DialogCreateNoteController,
				templateUrl: '/views/components/create-note.html',
				parent: angular.element(document.body),
				locals: {
					peopleList: PeopleService.people,
					notesList: vm.notesList
				},
				targetEvent: ev,
				clickOutsideToClose: true
			});
		};

		vm.init = function() {
			vm.loadData();
		};

		vm.init();
	};

	/** @ngInject */
	function DialogCreateNoteController($scope, $mdDialog, peopleList, notesList, NotesService, FlashService) {
		$scope.hide = function() {
			$mdDialog.hide();
		};
		$scope.cancel = function() {
			$mdDialog.cancel();
		};

		$scope.addFileLink = function() {
			$scope.newNote.files.push(angular.copy($scope.fileTemp));
			$scope.fileTemp = "";
		};

		$scope.removeFileLink = function(index) {
			$scope.newNote.files.splice(index, 1);
		};

		$scope.saveNote = function() {
			var idGenerate = $scope.notesList.length + 200;
			$scope.newNote.id = idGenerate.toString();
			$scope.newNote.owner = "1";
			$scope.newNote.post_time = moment().format('DD/MM/YYYY').toString();
			$scope.notesList.push(angular.copy($scope.newNote));

			$scope.newNote.files = $scope.newNote.files.join(",");
			$scope.newNote.folders = $scope.newNote.folders.join(",");

			var sharesTemp = [];
			angular.forEach($scope.newNote.shares, function(value, index) {
				sharesTemp.push(value.id);
			});
			$scope.newNote.shares = sharesTemp.join(",");

			NotesService.create($scope.newNote).then(function(resultCreate) {
				FlashService.message("Note Created!")
				$scope.cancel();
			}, function(error) {
				FlashService.message("Error on save note!")
			});

			$scope.hide();
		};

		$scope.initObject = function() {
			$scope.newNote = {};
			$scope.newNote.folders = [];
			$scope.newNote.shares = [];
			$scope.newNote.files = [];
		};

		$scope.init = function() {
			$scope.initObject();
			$scope.peopleList = peopleList;
			$scope.notesList = notesList;
		};

		$scope.init();
	};

})();
