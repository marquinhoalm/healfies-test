(function() {
	'use strict';

	angular.module('app').controller('MainController', MainController);

	/** @ngInject */
	function MainController(MainService, PeopleService) {
		var vm = this;
		// Fixed id user 1

		vm.init = function() {
			// MainService.setCurrentUser(PeopleService.getPerson("1"));
			vm.currentUser = MainService.getCurrentUser();
		};

		vm.init();
	}
})();
