(function() {

	'use strict';

	angular.module('app').config(ConfigRoutes);

	ConfigRoutes.$inject = ['$stateProvider', '$urlRouterProvider'];

	/* @ngInject */
	function ConfigRoutes($stateProvider, $urlRouterProvider) {

		var states = {

			'index': {
				url: '/note/list'
			},

			'main': {
				abstract: true,
				templateUrl: '/views/main.html',
				controller: 'MainController',
				controllerAs: 'main'
			},

			'main.notes': {
				url: '/notes',
				abstract: true
			},

			'main.notes.list': {
				url: '/list',
				views: {
					'content@main': {
						templateUrl: '/views/notes/list.html',
						controller: 'NotesListController',
						controllerAs: 'vm'
					}
				}
			}
		};

		angular.forEach(states, function(stateConfig, stateName) {
			$stateProvider.state(stateName, stateConfig);
		});

		$urlRouterProvider.otherwise('/notes/list');
	};
})();
